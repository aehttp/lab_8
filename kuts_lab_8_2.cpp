/*file name: kuts_lab_8_2 
*студент: Куць Ганна Олександрівна
*група : КН-1-3
*дата створення: 15.12.2021
*дата останньої зміни 15.12.2021
*Лабораторна №8
*завдання : Розробити блок-схему алгоритму та реалізувати його мовою С\С++ для обробки елементів в рядках чи у стовпчиках матриці
*призначення програмного файлу: Створити та вивести одномірний масив з сум від‘ємних елементів кожної діагоналі, що паралельні головній діагоналі та розташовані під нею 
*варіант : №4
*/
#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <math.h>
#include <time.h>
#include <stdlib.h>
using namespace std;
int main(){
system("cls");
SetConsoleCP(1251);
SetConsoleOutputCP(1251);
int n,m,j,i, pi,pj;
cout<<"Input n & m = ";
cin>>n;
cin>>m;
cout<<endl;
int a[n][m],k=0, sum=0;
int s[n];
srand(time(NULL));
int *m_p;
m_p=&a[0][0];
for(i=0;i<n;i++){
  for(j=0;j<m;j++){
    *m_p = rand()%100-50;
    printf("%4d",*m_p);  
    m_p++;
  }
  cout<<endl;
}
cout<<endl;
int *p;
p=&s[0];
  for(i=0; i<n ; i++) 
  {
    *p=0;
    p++;
  }
p=&s[0];
for (k = 0; k < n; k++)  
{
      pi=k; pj=0;
      for (; (pi<n)&(pj<m);pi++,pj++){
        m_p=&a[pi][pj];
        if (0>*m_p) 
        { 
          *p+=*m_p;
        }
      }
      p++;
}
  printf(" вихіднй масив: \n");
  p=&s[0];
for (i=0; i<n; i++)
{
    printf("%4d",*p);
    p++;
}
cout<<endl;
system("pause");
return 0;
}
