/*file name: kuts_lab_8_1 
*студент: Куць Ганна Олександрівна
*група : КН-1-3
*дата створення: 15.12.2021
*дата останньої зміни 15.12.2021
*Лабораторна №8
*завдання : Розробити блок-схему алгоритму та реалізувати його мовою С\С++ для обробки елементів в рядках чи у стовпчиках матриці
*призначення програмного файлу: Обчислити та вивести кількість непарних від‘ємних елементів масиву .
*варіант : №4
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <stdlib.h>
#include <time.h>
using namespace std;
int main() {
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int i,n , k=0;
	srand(time(NULL));
	cout<<"Задайте розмір матриці n = \t";
	cin>>n;
	int c7[n];
	int *p; 
	p=&c7[0];
	cout<<"\n Массив c7(n) = ";
	for (i=0;i<n;i++){
		 *p = rand()%100-40;
		 printf("%4d",*p);
		 p++;
		}
	p=&c7[0];
	for (i=0;i<n;i++){
		 if(*p%2 != 0 && *p<0) {
		 	k++;
		 }
		 p++;
		}
	printf("\n Кількість непарних від'ємних= %d \n",k);
	system("pause");
	return 0;
}
